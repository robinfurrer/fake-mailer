<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 11.04.2019
 * Time: 15:36
 */

namespace ch\clx\fakemailer\controller;


use ch\clx\fakemailer\config\Config;
use ch\clx\fakemailer\converter\MailConverter;
use ch\clx\fakemailer\entity\Mail;

class MailController
{

    /**
     * @var \mysqli
     */
    private $conn;

    private $converter;

    function __construct()
    {
        $this->converter = new MailConverter();
        $this->conn = Config::getDatabaseConnection();
    }

    /**
     * @param $email_id integer
     * @return \ch\clx\fakemailer\entity\Mail|null
     */
    public function getEmailById($email_id) {
        $stmt = $this->conn->prepare("SELECT * FROM email WHERE email_id = ?");
        $stmt->bind_param("i", $email_id);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows === 0) {
            $stmt->close();
            return null;
        }
        $stmt->close();
        return $this->converter->rowToEntity($result->fetch_assoc());
    }

    /**
     * @param $fk_user_id integer
     * @return array|null
     */
    public function getEmailByUser($fk_user_id) {
        $stmt = $this->conn->prepare("SELECT * FROM email WHERE fkUser = ?");
        $stmt->bind_param("i", $fk_user_id);
        $stmt->execute();
        $result = $stmt->get_result();
        $emails = null;
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $emails[] = $this->converter->rowToEntity($row);
            }
        }
        return $emails;
    }

    /**
     * @param $email Mail
     * @param bool $save
     * @return bool
     */
    public function createEmail($email, $save = true)
    {
        if ($save) {
            // because prepared statements sucks
            $var1 = $email->getReceiverEmail();
            $var2 = $email->getEmailSubject();
            $var3 = $email->getEmailContent();
            $var4 = $email->getSenderName();
            $var5 = $email->getSenderEmail();
            $var6 = $email->getEmailReplyToEmail();
            $loggedInUser = $_SESSION['userId'];

            if ($loggedInUser == null) {
                return false;
            }

            $sent = $this->sendMail($email);

            $stmt = $this->conn->prepare("INSERT INTO email (fkUser, receiverEmail, emailSubject, emailContent, senderName, senderEmail, emailReplyToEmail, sended) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("issssssi", $loggedInUser, $var1, $var2, $var3, $var4, $var5, $var6, $sent);
            $stmt->execute();
        } else {
            $sent = $this->sendMail($email);
        }
        return $sent;
    }

    /**
     * @param $email Mail
     * @return bool
     */
    private function sendMail($email) {
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From: " . $email->getSenderName() . " <" . $email->getSenderEmail() . "> \r\n";
        if ($email->getEmailReplyToEmail() != null) {
            $headers .= "Reply-To: " . $email->getSenderEmail() . " <" . $email->getEmailReplyToEmail() . "> \r\n";
        }

        try {
            return mail($email->getReceiverEmail(), $email->getEmailSubject(), nl2br($email->getEmailContent()), $headers);
        } catch (\Exception $e) {
            return false;
        }
    }
}