<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 11.04.2019
 * Time: 14:52
 */

namespace ch\clx\fakemailer\controller;


use ch\clx\fakemailer\config\Config;
use ch\clx\fakemailer\converter\UserConverter;
use ch\clx\fakemailer\entity\Mail;

class UserController
{

    /**
     * @var \mysqli
     */
    private $conn;

    private $converter;

    function __construct()
    {
        $this->converter = new UserConverter();
        $this->conn = Config::getDatabaseConnection();
    }

    /**
     * @param $userId Integer
     * @return \ch\clx\fakemailer\entity\User|null
     */
    public function getUserById($user_id) {
        $stmt = $this->conn->prepare("SELECT * FROM user WHERE user_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows === 0) {
            $stmt->close();
            return null;
        }
        $stmt->close();
        return $this->converter->rowToEntity($result->fetch_assoc());
    }

    /**
     * @param $username string
     * @param $password string
     * @return \ch\clx\fakemailer\entity\User|null
     */
    public function login($emailaddress, $password) {
        $stmt = $this->conn->prepare("SELECT * FROM user WHERE emailaddress = ? AND password = ?");
        $stmt->bind_param("ss", $emailaddress, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows === 0) {
            $stmt->close();
            return null;
        }
        $stmt->close();
        $user = $this->converter->rowToEntity($result->fetch_assoc());
        if($user->isActivate()) {
            return $user;
        } else {
            throw new \Exception("User not activated.");
        }
    }

    /**
     * @param $emailaddress string
     * @param $password string
     * @return bool
     */
    public function register($emailaddress, $password) {
        $stmt = $this->conn->prepare("INSERT INTO user (emailaddress, password) VALUES (?, ?)");
        $stmt->bind_param("ss", $emailaddress,$password);
        $stmt->execute();
        $id = $this->conn->insert_id;

        // TODO: code so ugly sorry for that

        if(($error = $stmt->error) == null){
            $this->createUserActivationKey($id, $emailaddress);
            return true;
        } else {
            $bullshitArray = explode("'", $error);
            return $bullshitArray[sizeof($bullshitArray ) - 2];
        }
    }

    private function createUserActivationKey($fkUser, $emailAddress) {
        $hextime = md5(time());
        $stmt = $this->conn->prepare("INSERT INTO user_activation_key (fkUser, activation_key) VALUES (?, ?)");
        $stmt->bind_param("is",$fkUser, $hextime);
        $stmt->execute();

        $email = new Mail();
        $email->setEmailSubject("Confirm your email address");
        $email->setEmailContent("Please Confirm Registration<br><br><a href='http://fake-mailer.ga/Confirm.php?key=" . $hextime . "'>confirm here</a><br><br>If you received this email by mistake, simply delete it. You won't be registered if you don't click the confirmation link above.<br><br>For questions about this, please contact me:<br><a href='mailto:robin.furrer2000@gmail.com'>robin.furrer2000@gmail.com</a>");
        $email->setSenderName("fake-mailer by Robin Furrer");
        $email->setSenderEmail("no-reply@fake-mailer.ga");
        $email->setReceiverEmail($emailAddress);
        $mailController = new MailController();
        $mailController->createEmail($email, false);
    }

    public function activateUserKey($key) {
        $stmt = $this->conn->prepare("SELECT fkUser FROM user_activation_key WHERE activation_key = ?");
        $stmt->bind_param("s", $key);
        $stmt->execute();
        $result = $stmt->get_result();
        $user = null;
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $user = $row['fkUser'];
            }
        } else {
            return null;
        }
        $this->activateUser($user);
        $stmt2 = $this->conn->prepare("DELETE FROM user_activation_key WHERE activation_key = ?");
        $stmt2->bind_param("s", $key);
        $stmt2->execute();

        return true;
    }

    private function activateUser($user) {
        $stmt = $this->conn->prepare("UPDATE user SET active = '1' WHERE user_id = ?");
        $stmt->bind_param("i",$user);
        $stmt->execute();
    }
}