<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 11.04.2019
 * Time: 14:40
 */

namespace ch\clx\fakemailer\config;


class Config
{
    /**
     * @var \mysqli
     */
    private static $conn;

    /**
     * @return array
     */
    private static function getDatabaseLogin() {
        return [
            'server' => "127.0.0.1",
            'username' => "root",
            'password' => "",
            'database' => "fake-mailer-db",
            'port' => 3306
        ];
    }

    /**
     * return \mysqli
     */
    public static function getDatabaseConnection() {
        if(Config::$conn == null) {
            $db = Config::getDatabaseLogin();
            Config::$conn =  new \mysqli($db['server'], $db['username'], $db['password'], $db['database'], $db['port']);
        }
        return Config::$conn;
    }
}