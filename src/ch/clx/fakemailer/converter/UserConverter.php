<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 11.04.2019
 * Time: 15:00
 */

namespace ch\clx\fakemailer\converter;


use ch\clx\fakemailer\entity\User;

class UserConverter
{
    public function rowToEntity($row) {
        $user = new User();
        $user->setUserId($row['user_id']);
        $user->setEmailaddress($row['emailaddress']);
        $user->setUsername($row['username']);
        $user->setPassword($row['password']);
        if (isset($row['active'])) {
            $user->setActivate($row['active']);
        }
        return $user;
    }
}