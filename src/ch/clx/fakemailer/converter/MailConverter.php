<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 11.04.2019
 * Time: 15:36
 */

namespace ch\clx\fakemailer\converter;


use ch\clx\fakemailer\controller\UserController;
use ch\clx\fakemailer\entity\Mail;

class MailConverter
{
    /**
     * @var UserController
     */
    private $userController;

    function __construct()
    {
        $this->userController = new UserController();
    }

    public function rowToEntity($row) {
        $mail = new Mail();
        $mail->setEmailId($row['email_id']);
        $mail->setFkUser($this->userController->getUserById($row['fkUser']));
        $mail->setReceiverEmail($row['receiverEmail']);
        $mail->setEmailSubject($row['emailSubject']);
        $mail->setEmailContent($row['emailContent']);
        $mail->setSenderName($row['senderName']);
        $mail->setSenderEmail($row['senderEmail']);
        $mail->setEmailReplyToEmail($row['emailReplyToEmail']);
        $mail->setDate($row['date']);
        $mail->setSended($row['sended']);
        return $mail;
    }
}