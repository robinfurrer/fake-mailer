<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 11.04.2019
 * Time: 14:21
 */

namespace ch\clx\fakemailer\entity;


class Mail
{
    /**
     * @var integer
     */
    private $email_id;
    /**
     * @var User
     */
    private $fkUser;
    /**
     * @var string
     */
    private $receiverEmail;
    /**
     * @var string
     */
    private $emailSubject;
    /**
     * @var string
     */
    private $emailContent;
    /**
     * @var string
     */
    private $senderName;
    /**
     * @var string
     */
    private $senderEmail;
    /**
     * @var string
     * not working for now
     */
    private $emailReplyToEmail;
    /**
     * @var integer
     */
    private $date;

    /**
     * @var boolean
     */
    private $sended;

    // getter and setter

    /**
     * @return int
     */
    public function getEmailId()
    {
        return $this->email_id;
    }

    /**
     * @param int $email_id
     */
    public function setEmailId($email_id)
    {
        $this->email_id = $email_id;
    }

    /**
     * @return User
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }

    /**
     * @param User $fkUser
     */
    public function setFkUser($fkUser)
    {
        $this->fkUser = $fkUser;
    }

    /**
     * @return string
     */
    public function getReceiverEmail()
    {
        return $this->receiverEmail;
    }

    /**
     * @param string $receiverEmail
     */
    public function setReceiverEmail($receiverEmail)
    {
        $this->receiverEmail = $receiverEmail;
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return $this->emailSubject;
    }

    /**
     * @param string $emailSubject
     */
    public function setEmailSubject($emailSubject)
    {
        $this->emailSubject = $emailSubject;
    }

    /**
     * @return string
     */
    public function getEmailContent()
    {
        return $this->emailContent;
    }

    /**
     * @param string $emailContent
     */
    public function setEmailContent($emailContent)
    {
        $this->emailContent = $emailContent;
    }

    /**
     * @return string
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * @param string $senderName
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;
    }

    /**
     * @return string
     */
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }

    /**
     * @param string $senderEmail
     */
    public function setSenderEmail($senderEmail)
    {
        $this->senderEmail = $senderEmail;
    }

    /**
     * @return string
     */
    public function getEmailReplyToEmail()
    {
        return $this->emailReplyToEmail;
    }

    /**
     * @param string $emailReplyToEmail
     */
    public function setEmailReplyToEmail($emailReplyToEmail)
    {
        $this->emailReplyToEmail = $emailReplyToEmail;
    }

    /**
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param int $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
    * @return bool
    */
    public function isSended()
    {
        return $this->sended;
    }

    /**
    * @param bool $sended
    */
    public function setSended($sended)
    {
        $this->sended = $sended;
    }
}