<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 11.04.2019
 * Time: 16:42
 */

class Login
{

    /**
     * @var \ch\clx\fakemailer\controller\UserController
     */
    private $userController;

    function __construct()
    {
        require __DIR__ . '/../autoload.php';

        $this->userController = new \ch\clx\fakemailer\controller\UserController();
        if (!isset($_POST['email'])) {
            $this->displayHTML();
        } else {
            $this->login($_POST['email'], $_POST['password']);
        }
    }


    function displayHTML() {
        include "template/login.php";
    }

    /**
     * @param $email string
     * @param $password string
     */
    function login($email, $password) {
        try {
            if (($user = $this->userController->login($email, $password)) != null) {
                $_SESSION['userId'] = $user->getUserId();
                header("location: Send.php");
            } else {
                header("location: Login.php?error=Login information wrong.");
            }
        } catch (Exception $exception){
            header("location: Login.php?error=" . $exception->getMessage());
        }
    }
}

new Login();