<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 11.04.2019
 * Time: 16:42
 */

class Confirm
{

    /**
     * @var \ch\clx\fakemailer\controller\UserController
     */
    private $userController;

    function __construct()
    {
        require __DIR__ . '/../autoload.php';

        $this->userController = new \ch\clx\fakemailer\controller\UserController();
        if (isset($_GET['key'])) {
            $text = $this->confirmEmail($_GET['key']);
            $this->displayHTML($text);
        } else {
            header("location: index.php");
        }
    }


    function displayHTML($text) {
        include "template/confirm.php";
    }

    /**
     * @param $key string
     * @return string
     */
    function confirmEmail($key) {
        if (($user = $this->userController->activateUserKey($key)) != null) {
            return "Registration success";
        } else {
            return "Registration failed, Key not correct";
        }
    }
}

new Confirm();