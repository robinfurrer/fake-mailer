<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 11.04.2019
 * Time: 16:42
 */

class Register
{

    /**
     * @var \ch\clx\fakemailer\controller\UserController
     */
    private $userController;

    function __construct()
    {
        require __DIR__ . '/../autoload.php';

        $this->userController = new \ch\clx\fakemailer\controller\UserController();
        if (!isset($_POST['email'])) {
            $this->displayHTML();
        } else {
            $this->register($_POST['email'], $_POST['password']);
        }
    }


    function displayHTML() {
        include "template/register.php";
    }

    /**
     * @param $email string
     * @param $password string
     */
    function register($email, $password) {
        if (($user = $this->userController->register($email, $password)) === true) {
            header("location: Login.php?info=activateUser");
        } else {
            header("location: Register.php?error=$user");
        }
    }
}

new Register();