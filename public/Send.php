<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 11.04.2019
 * Time: 16:42
 */

class Send
{

    /**
     * @var \ch\clx\fakemailer\controller\MailController
     */
    private $mailController;

    function __construct()
    {
        require __DIR__ . '/../autoload.php';

        if(!isset($_SESSION['userId'])) {
            header("location: index.php");
        }

        $this->mailController = new \ch\clx\fakemailer\controller\MailController();
        if (!isset($_POST['receiverEmail'])) {
            $this->displayHTML();
        } else {
            $email = new \ch\clx\fakemailer\entity\Mail();
            $email->setReceiverEmail($_POST['receiverEmail']);
            $email->setEmailSubject($_POST['emailSubject']);
            $email->setEmailContent($_POST['emailContent']);
            $email->setSenderName($_POST['senderName']);
            $email->setSenderEmail($_POST['senderEmail']);
            if (isset($_POST['emailReplyToEmail'])) {
                $email->setEmailReplyToEmail($_POST['emailReplyToEmail']);
            }
            $this->send($email);
        }
    }


    function displayHTML() {
        include "template/sendEmail.php";
    }

    /**
     * @param $email \ch\clx\fakemailer\entity\Mail
     */
    function send($email) {
        if ($this->mailController->createEmail($email)) {
            include "template/success.php";
        } else {
            include "template/failed.php";
        }
    }
}

new Send();