<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>fake-mailer</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Quicksand:300'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway'>

    <style>
        /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
        body {
            overflow-x: hidden;
            overflow-y: hidden;
            color: white;
            font-family: "Raleway";
        }

        h3 {
            text-transform: uppercase;
            font-family: "Quicksand";
            text-align: center;
            color: white;
            font-size: 2.5em;
        }

        span {
            display: block;
            text-align: center;
        }

        hr {
            padding: 0;
            border: none;
            border-top: solid 3px white;
            text-align: center;
            max-width: 66%;
            margin: 25px auto 30px;
        }
        hr:after {
            content: "\f0e0";
            font-family: 'FontAwesome';
            display: inline-block;
            position: relative;
            top: -0.8em;
            font-size: 2em;
            padding: 0 0.25em;
            background-color: #4f4747;
            color: white;
        }

        .primary {
            background: #4f4747;
            height: 100vh;
            width: 100vw;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .floating-label-form-group label {
            display: block;
            z-index: 0;
            position: relative;
            top: 1.5em;
            margin: 0;
            font-size: 0.85em;
            line-height: 1em;
            vertical-align: middle;
            vertical-align: baseline;
            opacity: 0;
            transition: top 0.3s ease, opacity 0.3s ease;
        }

        .floating-label-form-group input,
        .floating-label-form-group textarea {
            z-index: 1;
            position: relative;
            padding-right: 0;
            padding-left: 0;
            border: none;
            border-radius: 0;
            font-size: 1em;
            background: none;
            box-shadow: none !important;
            resize: none;
        }

        .floating-label-form-group-with-value label {
            top: 0;
            opacity: 1;
        }

        .floating-label-form-group-with-focus label {
            color: #0091EA;
        }

        @keyframes launch {
            0% {
                opacity: 1;
                transform: translateX(0) scale(1);
            }
            10%, 15% {
                transform: translateX(0) scale(0.5);
            }
            30%, 35% {
                transform: translateX(-20) scale(0.5);
            }
            40%, 70% {
                transform: translateX(100vw) scale(0.5);
            }
            70% {
                opacity: 1;
            }
            71% {
                opacity: 0;
                transform: translateX(100vw) scale(0.5);
            }
            72% {
                opacity: 0;
                transform: translateX(0px) scale(0.5);
            }
            100% {
                opacity: 1;
                transform: translateX(0px) scale(1);
            }
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
</head>
<body>
    <section id="contact" class="primary">
        <div class="container">
            <h3>Email Sent!</h3>
            <hr>
        </div>
    </section>
    <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
    <script  src="js/index.js"></script>
</body>
</html>