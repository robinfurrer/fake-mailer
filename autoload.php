<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 11.04.2019
 * Time: 15:07
 */

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

spl_autoload_register(function ($class_name) {
    if($class_name == "Main"){
        $class = __DIR__ . "/public/" . $class_name . '.php';
        $class = str_replace('\\', '/', $class);
        require_once $class;
    } else {
        $class = __DIR__ . "/src/" . $class_name . '.php';
        $class = str_replace('\\', '/', $class);
        require_once $class;
    }
});